using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using DAL.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Api.Controllers
{
    public class AuthenticationController : BaseController
    {
        private readonly IUsuariosService usuariosService;
        private readonly IConfiguration configuration;

        public AuthenticationController(IUsuariosService usuariosService, IConfiguration configuration)
        {
            this.usuariosService = usuariosService;
            this.configuration = configuration;
        }

        /// <summary>
        /// Devuelve un token o un 401 unauthorized según al autenticidad del usuario y contraseña
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]LoginDTO login)
        {
            IActionResult response = Unauthorized();
            login.Password = login.Password.ComputeSha256Hash();
            var user = usuariosService.AutenticarUsuario(login);

            if (user != null)
            {
                var tokenString = user.CreateJsonWebToken(configuration);
                response = Ok(new { token = tokenString, Id = user.Id });
            }

            return response;
        }

        /// <summary>
        /// Devuelve un true o false dependiendo de si el usuario y contraseña estan correctos
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        // POST api/autenticacion
        [HttpPost("admin")]
        public IActionResult Post([FromBody]LoginWebDTO usuarioWeb)
        {
            var usuario = usuariosService.AutenticarUsuarioAdmin(usuarioWeb);
        
            if(usuario == null){
                return NotFound(usuario);
            }else if(!ModelState.IsValid){
                return BadRequest(ModelState);
            }else{
            return Ok(usuario);
            }
        }       
    }
}