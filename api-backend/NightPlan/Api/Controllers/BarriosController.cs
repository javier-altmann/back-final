using System.Collections.Generic;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class BarriosController : BaseController
    {
        IEstablecimientosService establecimientos;
        public BarriosController(IEstablecimientosService establecimientos)
        {
            this.establecimientos = establecimientos;
        }
        
        /// <summary>
        /// Trae todos los barrios
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        [Produces("application/json", Type = typeof(OperationResult<IEnumerable<OpcionesPreferenciasDTO>>))]
        public IActionResult Get()
        {
            var barrios = establecimientos.GetBarrios();
            return Ok(barrios);
        } 
    }
}