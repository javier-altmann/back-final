using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public abstract class BaseController : Controller
    {
        protected int UserId
        { 
            get
            {
                return User.GetUserId();
            }
        }
    }
}