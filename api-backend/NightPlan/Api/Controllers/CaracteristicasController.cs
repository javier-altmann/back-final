using System.Collections.Generic;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class CaracteristicasController : BaseController
    {
        IEstablecimientosService establecimientos;
        public CaracteristicasController(IEstablecimientosService establecimientos)
        {
            this.establecimientos = establecimientos;
        }

        /// <summary>
        /// Trae todas las caracteristicas
        /// </summary>
        /// <param></param>
        /// <returns></returns>    
        [HttpGet]
        [Produces("application/json", Type = typeof(OperationResult<IEnumerable<OpcionesPreferenciasDTO>>))]

        public IActionResult Get()
        {
            var caracteristicas = establecimientos.GetInstalaciones();
            if (caracteristicas == null)
            {
                return NotFound(caracteristicas);
            }

            return Ok(caracteristicas);
        }

    }
}