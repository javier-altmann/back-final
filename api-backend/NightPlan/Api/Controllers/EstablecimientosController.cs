using System.Collections.Generic;
using Core.DTO.CrearEstablecimientos;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class EstablecimientosController : BaseController
    {
        IEstablecimientosService establecimientos;
        public EstablecimientosController(IEstablecimientosService establecimientos)
        {
            this.establecimientos = establecimientos;
        }

        /// <summary>
        /// Devuelve los establecimientos destacados, según el offset y limit que se pase en los parametros
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public IActionResult Get(int limit, int offset)
        {
            var establecimientosDestacados = establecimientos.getEstablecimientosDestacados(limit, offset);
            if (establecimientosDestacados.Data == null)
            {
                NotFound(establecimientosDestacados);
            }
            return Ok(establecimientosDestacados);
        }


        /// <summary>
        /// Devuelve el establecimiento del id que se le pasa. También tiene la posibilidad de solicitarle el detalle del establecimiento
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET api/values
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var establecimiento = establecimientos.Get(id);
            
            return Ok(establecimiento);
        }

        /// <summary>
        /// Crea un establecimiento
        /// </summary>
        /// <param name="establecimiento"></param>
        /// <returns></returns>
        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]CrearEstablecimientosDTO establecimiento)
        {
            var establecimientoNuevo = establecimientos.CrearEstablecimientos(establecimiento);
             
            if (establecimientoNuevo.Data == null)
            {
                return NotFound(establecimientoNuevo);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return Created("", establecimientoNuevo);
            }            
        }
    }
}