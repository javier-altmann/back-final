using System.Collections.Generic;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class GastronomiaController : BaseController
    {
        IEstablecimientosService establecimientos;
        public GastronomiaController(IEstablecimientosService establecimientos)
        {
            this.establecimientos = establecimientos;
        }

        /// <summary>
        /// Trae todas las opciones de gastronomia
        /// </summary>
        /// <param></param>
        /// <returns></returns>    
        //api/gastronomia
        [HttpGet]
        [Produces("application/json", Type = typeof(OperationResult<IEnumerable<OpcionesPreferenciasDTO>>))]
        public IActionResult Get()
        {
            var gastronomia = establecimientos.GetGastronomia();
            if (gastronomia == null)
            {
                NotFound(gastronomia);
            }
            return Ok(gastronomia);
        }
    }
}