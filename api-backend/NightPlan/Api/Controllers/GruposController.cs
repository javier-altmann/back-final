using System.Collections.Generic;
using System.Runtime.InteropServices;
using Core.DTO;
using Core.DTO.CrearGrupo;
using Core.Interfaces;
using Core.Services.ResponseModels;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class GruposController : BaseController
    {
        IGruposService gruposService;
        IRecomendadosService recomendadosService;

        public GruposController(IGruposService gruposService, IRecomendadosService recomendadosService)
        {
            this.gruposService = gruposService;
            this.recomendadosService = recomendadosService;
        }

        /// <summary>
        /// Devuelve los grupos del usuario que cumplan con el filtro
        /// </summary>
        /// <param name="search"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        // GET api/grupos
        [HttpGet("Own")]
        [ProducesResponseType(200, Type = typeof(OperationResult<IEnumerable<GruposDelUsuarioDTO>>))]
        [ProducesResponseType(404, Type = typeof(OperationResult<IEnumerable<GruposDelUsuarioDTO>>))]
        public IActionResult GetOwn(int limit, int offset, string search = "")
        {

            var resultadoDeBuscador = gruposService.GetSearchGroups(UserId, limit, offset, search);

            if (resultadoDeBuscador.Data == null)
            {
                return NotFound(resultadoDeBuscador);
            }
            else if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return Ok(resultadoDeBuscador);
            }
        }

        /// <summary>
        /// Devuelve los usuarios que pertenecen al grupo
        /// </summary>
        /// <param name="id">Id del grupo buscado</param>
        /// <param name="excludeMe">Hace que el usuario actual no esté incluido en la lista</param>
        /// <returns></returns>
        // GET api/grupos/5/usuarios
        [HttpGet("{id}/Usuarios")]
        [ProducesResponseType(200, Type = typeof(UsuarioDelGrupoResponse))]
        [ProducesResponseType(404, Type = typeof(UsuarioDelGrupoResponse))]
        public IActionResult Get(int id)
        {
            var usuariosDelGrupo = gruposService.GetUsuariosDelGrupoConResultado(id, UserId);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                return Ok(usuariosDelGrupo);
            }
        }

        /// <summary>
        /// Crea el grupo
        /// </summary>
        /// <param name="grupo"></param>
        /// <returns></returns>
        // POST api/grupos
        [HttpPost]
        public IActionResult Post([FromBody] GrupoDTO grupo)
        {
            var nuevoGrupo = gruposService.CrearGrupo(grupo, UserId);
            if (!ModelState.IsValid || grupo.emails.Length == 0)
            {
                return BadRequest(ModelState);
            }
            else if (nuevoGrupo.Succes)
            {
                return Created("api/grupos/", nuevoGrupo);
            }
            else
            {
                return NotFound(nuevoGrupo);
            }
        }

        [HttpGet("{id}/Recomendados")]
        [ProducesResponseType(200, Type = typeof(OperationResult<IList<EstablecimientoDTO>>))]
        public IActionResult GetRecomendados(int id)
        {
            var result = recomendadosService.GetEstablecimientosRecomendados(id);

            return Ok(result);
        }
    }
}