using Core.DTO;
using Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace Api.Controllers
{
    public class PruebaController : BaseController
    {
        private readonly IAuthenticationFacebookService authfbService;
        private readonly IConfiguration configuration;
        public PruebaController(IAuthenticationFacebookService authfbService, IConfiguration configuration)
        {
            this.authfbService = authfbService;
            this.configuration = configuration;
        }
        
        /// <summary>
        /// Prueba
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        // GET api/prueba
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> PostAsync([FromBody]Facebook fb)
        {
            IActionResult response = Unauthorized();

            var usuarios = await authfbService.Facebook(fb);
            
            if(usuarios != null){
                var tokenString = usuarios.CreateJsonWebToken(configuration);
                response = Ok(new { token = tokenString, Id = usuarios.Id });
            }
            return response;
        } 
    }
}