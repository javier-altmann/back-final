using System.Collections.Generic;
using Core.DTO; 
using Core.Interfaces;
using Core.Services.ResponseModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Api.Controllers
{
    public class UsuariosController : BaseController
    {
        private readonly IUsuariosService usuariosService;
        private readonly IGruposService gruposService;
        private readonly IConfiguration configuration;

        public UsuariosController(
            IUsuariosService usuariosService,
            IGruposService gruposService,
            IConfiguration configuracion)
        {
            this.usuariosService = usuariosService;
            this.gruposService = gruposService;
            this.configuration = configuracion;
        }

        /// <summary>
        /// Devuelve los datos de todos los usuarios de la app
        /// </summary>
        /// <returns></returns>
        // GET api/usuarios
        [HttpGet("{query?}")]
        public IActionResult Get(string query = null, bool excludeMe = true)
        {
            var usuarios = usuariosService.List(query, excludeMe ? UserId : null as int?);
            if (usuarios.Data == null)
            {
                return NotFound(usuarios);
            }
            return Ok(usuarios.Data);
        }

        /// <summary>
        /// Devuelve los datos del usuario logueado
        /// </summary>
        /// <returns></returns>
        // GET api/id/usuarios
        [HttpGet("Own")]
        public IActionResult GetOwn()
        {
            var usuarios = usuariosService.Get(UserId);
            if (usuarios.Data == null)
            {
                return NotFound(usuarios);
            }
            return Ok(usuarios.Data);
        }
        /// <summary>
        /// Devuelve los grupos del usuario
        /// </summary>
        /// <param name="id"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        // GET api/usuarios/5/grupos
        [HttpGet("{id}/grupos")]
        public IActionResult Get(int id, int limit, int offset)
        {
            var gruposDelUsuario = gruposService.ListByUsuario(id, limit, offset);
            if (gruposDelUsuario.Data == null)
            {
                return NotFound(gruposDelUsuario);
            }
            return Ok(gruposDelUsuario);
        }

        /// <summary>
        /// Guardar los datos del usuario 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        // POST api/usuarios
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(200, Type = typeof(UsuarioDTO))]
        [ProducesResponseType(404, Type = typeof(UsuarioDTO))]
        [ProducesResponseType(400, Type = typeof(UsuarioDTO))]
        public IActionResult Post([FromBody]UsuarioDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else if (user == null)
            {
                return NotFound(user);
            }

            user.Password = user.Password.ComputeSha256Hash();

            var usuario = usuariosService.Create(user);
            var tokenString = usuario.CreateJsonWebToken(configuration);
            
            return Created("", new { token = tokenString, Id = usuario.Id });
        }

        /// <summary>
        /// Actualizar los datos del usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        // PUT api/usuarios/5
        [HttpPut("Own")]
        [ProducesResponseType(204, Type = typeof(UsuarioDTO))]
        [ProducesResponseType(404, Type = typeof(UsuarioDTO))]
        [ProducesResponseType(400, Type = typeof(UsuarioDTO))]
        public IActionResult PutOwn([FromBody]UsuarioDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (user == null)
            {
                return NotFound(user);
            }   
            var usuario = usuariosService.Update(user);

            return NoContent();
        }
    }
}