﻿using System.IO;
using Core.Interfaces;
using Core.Services;
using DAL.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using DAL;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(); 
            services.AddCors();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,"ApiNightPlan.xml");
            services.AddSwaggerGen(c=>{
                c.IncludeXmlComments(filePath);
            });

            var connection = Configuration.GetConnectionString("NightPlanMysql");      
           
            services.AddDbContext<NightPlanDbContext>(options => options.UseMySql(connection));
            services.AddScoped<IGruposService, GruposService>();
            services.AddScoped<IEstablecimientosService, EstablecimientosService>();
            services.AddScoped<IUsuariosService, UsuariosService>();
            services.AddScoped<IRecomendadosService, RecomendacionesService>();
            services.AddScoped<IPreferenciasService, PreferenciasService>();
            services.AddScoped<IVotacionService, VotacionService>();
            services.AddScoped<IAuthenticationFacebookService, AuthenticationFacebookService>();

            services.ConfigureJwtAuthentication(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "MyAPI V1");
                });
            }
            app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials());  
            app.UseMvc();
            app.UseMvc(routes =>
            {
                routes.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
