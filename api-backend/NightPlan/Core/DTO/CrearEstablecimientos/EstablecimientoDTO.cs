namespace Core.DTO
{
    public class EstablecimientoDTO : BaseDTO
    {
        public string Nombre { get; set; }
        public string Imagen { get; set; }
        public string Direccion { get; set; }
        public bool Destacado { get; set; }
    }

    public class EstablecimientoConScoringDTO : EstablecimientoDTO
    {
        public int Score { get; set; }
    }
}