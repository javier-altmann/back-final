using System.ComponentModel.DataAnnotations;

namespace Core.DTO.CrearGrupo
{
    public class GrupoDTO
    {
        [Required]
        public string nombre { get; set; }
        public string Imagen { get; set; }

        [Required(ErrorMessage="El campo emails es obligatorio")]
        public string[] emails { get; set; }
    }
}