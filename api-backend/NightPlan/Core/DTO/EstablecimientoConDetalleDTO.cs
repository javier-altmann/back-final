using System.Collections.Generic;
using DAL.Model;

namespace Core.DTO
{
    public class EstablecimientoConDetalleDTO : EstablecimientoDestacadosDTO
    {
        public string[] Caracteristicas { get; set; }
        public string[] Gastronomias { get; set; }
    }
}