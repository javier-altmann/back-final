namespace Core.DTO
{
    public class EstablecimientoDestacadosDTO : BaseDTO
    {
        public string Nombre { get; set; }
        public string Imagen { get; set; }
        public string Direccion { get; set; }
        public bool Destacado { get; set; }        
    }
}