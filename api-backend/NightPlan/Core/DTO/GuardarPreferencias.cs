using System.Linq;

namespace Core.DTO
{
    public class GuardarPreferencias
    {
        public int GrupoId {get;set;}
        public PreferenciasDTO Respuesta { get; set; }
        public int[] IdsPreferencias
        {
            get
            {
                return Respuesta.IdsCaracteristicas
                    .Concat(Respuesta.IdsGastronomia).ToArray();
            }
        }
        public int? ContadorDePreferencias { get; set; }
    }
}