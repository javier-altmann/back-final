using System.ComponentModel.DataAnnotations;

namespace Core.DTO
{
    public class LoginDTO
    {
        [Required(ErrorMessage="Falta ingresar el email")]
        public string Email { get; set; }
        
        [Required(ErrorMessage="Falta ingresar la contraseña")]
        public string Password { get; set; }
    }
}