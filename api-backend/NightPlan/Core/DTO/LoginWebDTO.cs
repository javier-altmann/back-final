using System.ComponentModel.DataAnnotations;

namespace Core.DTO
{
    public class LoginWebDTO
    {
        [Required(ErrorMessage="Falta ingresar el email")]
        public string Username { get; set; }
        
        [Required(ErrorMessage="Falta ingresar la contraseña")]
        public string Password { get; set; }
    }
}