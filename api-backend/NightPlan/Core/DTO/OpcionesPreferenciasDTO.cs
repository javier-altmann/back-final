namespace Core.DTO
{
    public class OpcionesPreferenciasDTO : BaseDTO
    {
        public string Nombre { get; set; }
    }
}