using System;
using System.ComponentModel.DataAnnotations;
using Core.Validation;
using DAL.Model;

namespace Core.DTO
{
    [UsuarioValidatorAttribute]
    public class UsuarioDTO : BaseDTO
    {
        [Required(ErrorMessage="El nombre es obligatorio")]
        public string Nombre { get; set; }
        public string Apellido { get; set; }        
        public string Email { get; set; }
        public string ImagenPerfil { get; set; }
        public string Password { get; set; }
        public DateTime? FechaNacimiento { get; set; }
    }
}