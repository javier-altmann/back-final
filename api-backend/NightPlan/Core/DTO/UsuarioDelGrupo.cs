namespace Core.DTO
{
    public class UsuarioDelGrupo : BaseDTO
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string ImagenPerfil { get; set; }
        public bool Resultado { get; set; }
    }
}