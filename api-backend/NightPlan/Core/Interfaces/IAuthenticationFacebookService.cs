using System.Threading.Tasks;
using Core.DTO;
using DAL.Model;

namespace Core.Interfaces
{
    public interface IAuthenticationFacebookService
    {
        Task<Usuario> Facebook(Facebook fb);
    }
}