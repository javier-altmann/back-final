using Core.DTO;
using Core.Services.ResponseModels;
using System.Collections.Generic;
using Core.DTO.CrearEstablecimientos;

namespace Core.Interfaces
{
    public interface IEstablecimientosService
    {
         OperationResult<IEnumerable<EstablecimientoDestacadosDTO>> getEstablecimientosDestacados(int limit,int offset);
         EstablecimientoConDetalleDTO Get(int id);
         IEnumerable<OpcionesPreferenciasDTO> GetInstalaciones();
         IEnumerable<OpcionesPreferenciasDTO> GetBarrios();
         IEnumerable<OpcionesPreferenciasDTO> GetGastronomia();
         PostResult<CrearEstablecimientosDTO> CrearEstablecimientos(CrearEstablecimientosDTO establecimiento);
    }
}