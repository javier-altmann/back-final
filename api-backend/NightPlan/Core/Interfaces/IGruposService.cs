using System;
using System.Collections.Generic;
using Core.DTO.CrearGrupo;
using Core.DTO;
using Core.Services.ResponseModels;


namespace Core.Interfaces
{
    public interface IGruposService
    {
        //Obtiene los grupos que pertenecen al usuario
         OperationResult<IEnumerable<GruposDelUsuarioDTO>> ListByUsuario(int idUsuario, int limit, int offset);

         //Obtiene los usuario de cada grupo que tiene el usuario que hace la petición
         UsuarioDelGrupoResponse GetUsuariosDelGrupoConResultado(int grupoId, int loggedUserId);

        // Filtra los grupos del usuario según lo que se ingrese en el buscador
        OperationResult<IEnumerable<GruposDelUsuarioDTO>> GetSearchGroups(int idUsuario, int limit, int offset, string search);
    
        OperationResult<IEnumerable<ResponseUsuarioDTO>> getUsuarios(string email);

        CrearGrupoResponseApi CrearGrupo(GrupoDTO grupo, int creatorUserId);
    }
}