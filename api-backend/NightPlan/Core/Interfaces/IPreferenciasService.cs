using Core.DTO;
using Core.Services.ResponseModels;

namespace Core.Interfaces
{
    public interface IPreferenciasService
    {
        OperationResult<GuardarPreferencias> GuardarPreferencia(GuardarPreferencias preferencias, int loggedUserId);
    }
}