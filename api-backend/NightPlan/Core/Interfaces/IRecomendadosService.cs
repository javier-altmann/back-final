using System.Collections.Generic;
using Core.DTO;
using Core.Services.ResponseModels;

namespace Core.Interfaces
{
    public interface IRecomendadosService
    {
        OperationResult<IEnumerable<EstablecimientoDTO>> GetEstablecimientosRecomendados(int grupoId);   
    }
}