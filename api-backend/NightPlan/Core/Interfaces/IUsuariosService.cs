using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DTO;
using Core.Services.ResponseModels;
using DAL.Model;

namespace Core.Interfaces
{
    public interface IUsuariosService
    {
          Usuario AutenticarUsuario(LoginDTO usuario);
          OperationResult<UsuarioDTO> Get(int id);
          Usuario Create(UsuarioDTO user);
          PostResult<UsuarioDTO> Update(UsuarioDTO user);
          LoginResponseApi AutenticarUsuarioAdmin(LoginWebDTO usuario);
          OperationResult<IEnumerable<UsuarioDTO>> List(string query = null, int? ecludeUserId = null);
    }
}