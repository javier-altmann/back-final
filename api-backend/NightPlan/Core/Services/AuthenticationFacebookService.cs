using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Core.DTO;
using Core.Interfaces;
using DAL;
using DAL.Model;
using Newtonsoft.Json;
using static Core.DTO.FacebookApiResponses;

namespace Core.Services
{
    public class AuthenticationFacebookService : IAuthenticationFacebookService
    {
        private NightPlanDbContext context;
        //private readonly FacebookAuthSettings fbAuthSettings;
        private static readonly HttpClient Client = new HttpClient ();
        public AuthenticationFacebookService(NightPlanDbContext context)
        {
            this.context = context;
          //  this.fbAuthSettings = fbAuthSettings;
        }

        public async Task<Usuario> Facebook(Facebook fb) {
                var AppId = "2118765051513343";
                var AppSecret = "d902fe567344e5769a66972cca6d1051";

                var appAccessTokenResponse = await Client.GetStringAsync ($"https://graph.facebook.com/oauth/access_token?client_id={AppId}&client_secret={AppSecret}&grant_type=client_credentials");
                var appAccessToken = JsonConvert.DeserializeObject<FacebookAppAccessToken> (appAccessTokenResponse);

                // 2. validate the user access token
                var userAccessTokenValidationResponse = await Client.GetStringAsync ($"https://graph.facebook.com/debug_token?input_token={fb.AccessToken}&access_token={appAccessToken.AccessToken}");

                var userAccessTokenValidation = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidation> (userAccessTokenValidationResponse);

                // 3. we've got a valid token so we can request user data from fb
                var userInfoResponse = await Client.GetStringAsync ($"https://graph.facebook.com/v3.2/me?fields=id,email,first_name,last_name,name,gender,locale,birthday,picture&access_token={fb.AccessToken}");
                var userInfo = JsonConvert.DeserializeObject<FacebookUserData> (userInfoResponse);

                var user = context.Usuarios.SingleOrDefault(x => x.Email == userInfo.Email);

                if (user == null) {
                    var appUser = UserDataToModel(userInfo);

                    context.Usuarios.Add(appUser);
                    context.SaveChanges();
                    return appUser;
                }else{ 
                      
                    return user;
                }              
        }
            private Usuario UserDataToModel (FacebookUserData usuarioDto, Usuario instance = null) {
            instance = instance ?? new Usuario ();
            instance.Id = instance.Id;
            instance.Nombre = usuarioDto.FirstName;
            instance.Apellido = usuarioDto.LastName;
            instance.Email = usuarioDto.Email;
            instance.FechaNacimiento = usuarioDto.Birthday;
            instance.ImagenPerfil = usuarioDto.Picture.Data.Url;

            return instance;
        }

    }
}