using DAL.Model;
using Core.DTO;
using Core.Interfaces;
using System.Collections.Generic;
using Core.Services.ResponseModels;
using System.Linq;
using System;
using Core.DTO.CrearEstablecimientos;
using DAL;
using Microsoft.EntityFrameworkCore;

namespace Core.Services
{
    public class EstablecimientosService : IEstablecimientosService
    {
        private NightPlanDbContext context;
        public EstablecimientosService(NightPlanDbContext context)
        {
            this.context = context;
        }
        
        public OperationResult<IEnumerable<EstablecimientoDestacadosDTO>> getEstablecimientosDestacados(int limit, int offset)
        {
            var establecimientosDestacados = context.Establecimientos.Where(x => x.Destacado == true)
                                            .Select(y => new EstablecimientoDestacadosDTO
                                            {
                                                Id = y.Id,
                                                Nombre = y.Nombre,
                                                Imagen = y.Imagen,
                                                Direccion = y.Direccion,
                                                Destacado = y.Destacado
                                            }).OrderBy(ordenar => ordenar.Id)
                                            .Skip(offset).Take(limit).ToList();
            
            return new OperationResult<IEnumerable<EstablecimientoDestacadosDTO>> {
                Data = establecimientosDestacados
            };
        }

        public EstablecimientoConDetalleDTO Get(int id){
            var establecimiento = context.Establecimientos
                .Include(x=> x.EstablecimientoCaracteristicas)
                    .ThenInclude(x=> x.Caracteristica)
                    .Single(x=> x.Id == id);

            return new EstablecimientoConDetalleDTO
            {
                Id = establecimiento.Id,
                Nombre = establecimiento.Nombre,
                Direccion = establecimiento.Direccion,
                Destacado = establecimiento.Destacado,
                Imagen = establecimiento.Imagen,
                Gastronomias =  GetNombresPreferencias<Gastronomia>(establecimiento),
                Caracteristicas = GetNombresPreferencias<Instalacion>(establecimiento)
            };
        }

        private string[] GetNombresPreferencias<TPreferencia>(Establecimiento establecimiento)
            where TPreferencia: Caracteristica
        {
            return establecimiento
                    .EstablecimientoCaracteristicas
                    .Where(x => x.Caracteristica.GetType() == typeof(TPreferencia))
                    .Select(x => x.Caracteristica.Nombre).ToArray();
        }

        public IEnumerable<OpcionesPreferenciasDTO> GetGastronomia()
        {
            var gastronomia = context.Gastronomias.Select(x => new OpcionesPreferenciasDTO()
            {
                Id = x.Id,
                Nombre = x.Nombre
            }).ToList();
            return gastronomia;
        }

        public IEnumerable<OpcionesPreferenciasDTO> GetBarrios()
        {
            var barrios = context.Barrios.Select(x => new OpcionesPreferenciasDTO()
            {
                Id = x.Id,
                Nombre = x.Nombre
            }).ToList();

            return barrios;
        }

        public IEnumerable<OpcionesPreferenciasDTO> GetInstalaciones()
        {
            var instalaciones = context.Instalaciones.Select(x => new OpcionesPreferenciasDTO()
            {
                Id = x.Id,
                Nombre = x.Nombre
            }).ToList();
            return instalaciones;

        }

        private void AddEstablecimientoPreferenicas(IEnumerable<int> idsPreferencia, Establecimiento establecimiento)
        {
            var establecimientosPreferencias = context.Caracteristicas.Where(x => idsPreferencia.Contains(x.Id))
                .Select(preferencia => new EstablecimientoCaracteristica
                {
                    Caracteristica = preferencia,
                    Establecimiento = establecimiento,
                }).ToList();

            context.EstablecimientosCaracteristicas.AddRange(establecimientosPreferencias);
        }

        private void AddEstablecimientosBarrios(IEnumerable<int> idsBarrios, Establecimiento establecimiento)
        {
            AddEstablecimientoPreferenicas(idsBarrios, establecimiento);
        }

        private void AddEstablecimientosCaracteristicas(IEnumerable<int> idsCaracteristicas, Establecimiento establecimiento)
        {
            AddEstablecimientoPreferenicas(idsCaracteristicas, establecimiento);
        }

        private void AddEstablecimientosGastronomia(IEnumerable<int> idsGastronomia, Establecimiento establecimiento)
        {
            AddEstablecimientoPreferenicas(idsGastronomia, establecimiento);
        }

        private Establecimiento AddEstablecimientos(EstablecimientoDTO establecimiento)
        {
            var datosDelEstablecimiento = new Establecimiento
            {
                Nombre = establecimiento.Nombre,
                Direccion = establecimiento.Direccion,
                Imagen = establecimiento.Imagen,
                Destacado = establecimiento.Destacado
            };
            context.Establecimientos.Add(datosDelEstablecimiento);
            context.SaveChanges();
            return datosDelEstablecimiento;
        }

        public PostResult<CrearEstablecimientosDTO> CrearEstablecimientos(CrearEstablecimientosDTO establecimiento)
        {
            try
            {
                var datosDelEstablecimiento = AddEstablecimientos(establecimiento.Establecimiento);

                var listaBarrios = establecimiento.Barrio.IdBarrio.ToList();

                AddEstablecimientosBarrios(listaBarrios, datosDelEstablecimiento);

                var listaGastronomia = establecimiento.Gastronomia.IdGastronomia.ToList();

                AddEstablecimientosGastronomia(listaGastronomia, datosDelEstablecimiento);

                var listaCaracteristicas = establecimiento.Caracteristicas.IdCaracteristica.ToList();
                AddEstablecimientosCaracteristicas(listaCaracteristicas, datosDelEstablecimiento);

                context.SaveChanges();
                var responseEstablecimiento = new PostResult<CrearEstablecimientosDTO>
                {
                    Data = establecimiento,
                };

                return responseEstablecimiento;
            }
            catch (Exception ex)
            {
                var responseEstablecimiento = new PostResult<CrearEstablecimientosDTO>
                {
                    MensajePersonalizado = ex.Message
                };
                return responseEstablecimiento;
            }
        }
    }
}