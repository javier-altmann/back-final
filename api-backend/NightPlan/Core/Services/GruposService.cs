using System.Collections.Generic;
using DAL.Model;
using Core.DTO;
using Core.DTO.CrearGrupo;
using Core.Interfaces;
using System.Linq;
using Core.Services.ResponseModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using DAL;

namespace Core.Services
{
    public class GruposService : IGruposService
    {
        private NightPlanDbContext context;
        public GruposService(NightPlanDbContext context)
        {
            this.context = context;
        }

        public OperationResult<IEnumerable<GruposDelUsuarioDTO>> ListByUsuario(int idUsuario, int limit, int offset)
        {
            var gruposDelUsuarioFiltrados = context.Usuarios
               .Where(x => x.Id == idUsuario)
               .Include(x => x.GruposUsuarios)
               .SelectMany(x => x.GruposUsuarios)
               .Include(x => x.Grupo)
               .Select(x => new GruposDelUsuarioDTO()
               {
                   Id = x.Grupo.Id,
                   FechaCreacion = x.Grupo.FechaCreacion.ToString(),
                   Nombre = x.Grupo.Nombre,
                   ImagenPerfil = x.Grupo.Imagen

               }).Take(limit).Skip(offset).ToList();
            var operationResult = new OperationResult<IEnumerable<GruposDelUsuarioDTO>>();


            if (gruposDelUsuarioFiltrados.Any())
            {
                operationResult.Data = gruposDelUsuarioFiltrados;
                return operationResult;
            }

            return operationResult;
        }

        private List<UsuarioDelGrupo> GetUsuariosDelGrupo(int idGrupo, int loggedUserId)
        {
            List<UsuarioDelGrupo> usuariosDelGrupo = context.Grupos.Where(x => x.Id == idGrupo)
                                         .Include(x => x.GruposUsuarios)
                                         .SelectMany(x => x.GruposUsuarios)
                                         .Include(x => x.Usuario)
                                         .Where(x => x.Usuario.Id != loggedUserId)
                                         .Select(x => new UsuarioDelGrupo()
                                         {
                                             Id = x.Usuario.Id,
                                             Nombre = x.Usuario.Nombre,
                                             Apellido = x.Usuario.Apellido,
                                             ImagenPerfil = x.Usuario.ImagenPerfil
                                          }).ToList();
            return usuariosDelGrupo;
        }

        private UsuarioDelGrupo GetLoggedUserRespuestaParaGrupo(int idGrupo, int loggedUserId)
        {
            var grupoUsuario = context.GruposUsuarios
                .Include(x => x.Grupo)
                .Include(x => x.Usuario)
                .Single(x => x.Grupo.Id == idGrupo &&
                    x.Usuario.Id == loggedUserId);
    
            return new UsuarioDelGrupo()
                {
                    Id = grupoUsuario.Usuario.Id,
                    Nombre = grupoUsuario.Usuario.Nombre,
                    Apellido = grupoUsuario.Usuario.Apellido,
                    Resultado = grupoUsuario.Respondio,
                };
        }

        #region Métodos que se utilizan para GetUsuariosDelGrupos 
        private UsuarioDelGrupoResponse MergeGroupUsers(List<UsuarioDelGrupo> usuariosDelGrupo, Dictionary<int, bool> usersResponses, UsuarioDelGrupo userLogged)
        {
            List<UsuarioDelGrupo> resultMergeUsuarioDelGrupo = usuariosDelGrupo.Select((usuario, i) =>
            {
                usuario.Resultado =  usersResponses.ContainsKey(usuario.Id) ? usersResponses[usuario.Id] : false;
                return usuario;
            }).ToList();

            return new UsuarioDelGrupoResponse()
            {
                Respuestas = resultMergeUsuarioDelGrupo,
                RespuestaLoguedUser = userLogged
            };
        }

        private Dictionary<int, bool> getUsersResponses(int idGrupo, int idUserLogged){
            var usersResponse = context.GruposUsuarios
                .Include(x => x.Usuario)
                .Where(x => x.Grupo.Id == idGrupo && x.Usuario.Id != idUserLogged)
                .ToDictionary(
                    grupoUsuario => grupoUsuario.Usuario.Id,
                    grupoUsuario => grupoUsuario.Respondio);
            return usersResponse;                                 
        }

        #endregion
        public UsuarioDelGrupoResponse GetUsuariosDelGrupoConResultado(int grupoId, int loggedUserId)
        {
            var usersResponses = getUsersResponses(grupoId, loggedUserId);
            List<UsuarioDelGrupo> usuariosDelGrupo = GetUsuariosDelGrupo(grupoId, loggedUserId);   
            UsuarioDelGrupo userLogged = GetLoggedUserRespuestaParaGrupo(grupoId, loggedUserId);
            UsuarioDelGrupoResponse usuariosDelGrupoConResultado = MergeGroupUsers(usuariosDelGrupo, usersResponses, userLogged);
            
            return usuariosDelGrupoConResultado;                              
        }
        
        public OperationResult<IEnumerable<GruposDelUsuarioDTO>> GetSearchGroups(int idUsuario, int limit, int offset, string search = "")
        {
            var gruposDelUsuario = context.Usuarios
                   .Where(x => x.Id == idUsuario)
                   .Include(x => x.GruposUsuarios)
                   .SelectMany(x => x.GruposUsuarios)
                   .Include(x => x.Grupo)
                   .Select(x => new GruposDelUsuarioDTO()
                   {
                       Id = x.Grupo.Id,
                       FechaCreacion = x.Grupo.FechaCreacion.ToString(),
                       Nombre = x.Grupo.Nombre,
                       ImagenPerfil = x.Grupo.Imagen
                   });            

            search = search == null ? string.Empty : search;
            
            var resultadoBusqueda = gruposDelUsuario
                .Where(grupo => grupo.Nombre.ToLower().Contains(search.ToLower()))
                .Skip(offset)
                .Take(limit);

            return new OperationResult<IEnumerable<GruposDelUsuarioDTO>>()
            {
                Data = resultadoBusqueda
            };
        }
        public OperationResult<IEnumerable<ResponseUsuarioDTO>> getUsuarios(string email)
        {
            var listaDeUsuarios = context.Usuarios.Where(x => x.Email == email)
                                                  .Select(user => new ResponseUsuarioDTO()
                                                  {
                                                      Mail = user.Email,
                                                      Id = user.Id
                                                  }).ToList();
            var operationResult = new OperationResult<IEnumerable<ResponseUsuarioDTO>>();
            if (listaDeUsuarios.Any())
            {
                operationResult.Data = listaDeUsuarios;
                return operationResult;
            }

            return operationResult;
        }

        private Grupo AddGrupo(GrupoDTO grupoDto)
        {
            Grupo grupo = new Grupo()
            {
                Nombre = grupoDto.nombre,
                Imagen = grupoDto.Imagen,
                FechaCreacion = DateTime.Now,
            };

            context.Grupos.Add(grupo);
            context.SaveChanges();
            return grupo;
        }

        private void AddGruposUsuarios(GrupoDTO grupoDto, Grupo grupo, int creatorUserId)
        {
            var creatorUser = context.Usuarios.Single(x => x.Id == creatorUserId);
            var emails = grupoDto.emails.ToList();
            emails.Add(creatorUser.Email);
            var usuarios = context.Usuarios.Where(x => emails.Contains(x.Email)).ToList();
            var gruposUsuarios = usuarios.Select(usuario => new GrupoUsuario
            {
                Usuario = usuario,
                Grupo = grupo,
            }).ToList();
            context.GruposUsuarios.AddRange(gruposUsuarios.ToList());
            context.SaveChanges();
        }

        private void AddEstadoDePreferencias(GrupoDTO grupoDto, Grupo grupo)
        {
            // Sumo 1 por el usuario creador
            var cantidadDeUsuarios = grupoDto.emails.Count() + 1;

            EstadoDePreferencia estadoDePreferencias = new EstadoDePreferencia
            {
                CantidadUsuariosPorGrupo = cantidadDeUsuarios,
                Grupo = grupo,
                ContadorPreferenciasElegidas = 0,
                ContadorDeVotos = 0
            };
            context.Add(estadoDePreferencias);
            context.SaveChanges();
        }

        public CrearGrupoResponseApi CrearGrupo(GrupoDTO grupoDto, int creatorUserId)
        {
            try
            {
                var grupo = AddGrupo(grupoDto);
                AddGruposUsuarios(grupoDto, grupo, creatorUserId);
                AddEstadoDePreferencias(grupoDto, grupo);
                return new CrearGrupoResponseApi(true, "Se guardo el grupo exitosamente");
            }
            catch (Exception ex)
            {
                return new CrearGrupoResponseApi(false, "Fallo al guardar los datos del grupo");
            }
        }
    }
}