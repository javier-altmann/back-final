using System;
using System.Collections.Generic;
using System.Linq;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using DAL.Model;
using DAL;
using Microsoft.EntityFrameworkCore;

namespace Core.Services
{
    public class PreferenciasService : IPreferenciasService
    {
        private NightPlanDbContext context;
        public PreferenciasService(NightPlanDbContext context)
        {
            this.context = context;
        }

        private EstadoDePreferencia GetEstadoDePreferencias(GuardarPreferencias preferenciasUsuario)
        {
            var estadoDeLasPreferencias = context.EstadoDePreferencias.Where(x => x.Grupo.Id == preferenciasUsuario.GrupoId).FirstOrDefault();

            return estadoDeLasPreferencias;
        }

        private void AddRespuestaUsuariosGrupos(GuardarPreferencias preferenciasDto, int loggedUserId)
        {
            var grupoUsuario = context.GruposUsuarios
                .Include(x => x.GruposUsuariosPreferencias)
                .SingleOrDefault(
                x => x.Grupo.Id == preferenciasDto.GrupoId &&
                x.Usuario.Id == loggedUserId);

            var preferencias = context.Caracteristicas.Where(preferencia =>
                preferenciasDto.IdsPreferencias.Contains(preferencia.Id));
                

            foreach(var preferencia in preferencias)
            {
                grupoUsuario.GruposUsuariosPreferencias.Add(new PreferenciaCaracteristica
                {
                    Caracteristica = preferencia,
                    GrupoUsuario = grupoUsuario
                });
            }

            var barrios = context.Barrios.Where(barrio =>
                preferenciasDto.Respuesta.IdsBarrios.Contains(barrio.Id));

            foreach(var barrio in barrios)
            {
                grupoUsuario.GruposUsuariosPreferencias.Add(new PreferenciaBarrial
                {
                    Barrio = barrio,
                    GrupoUsuario = grupoUsuario,
                });
            }
            grupoUsuario.Respondio = true;
        }

        private GuardarPreferencias UpdateContadorDePreferencias(EstadoDePreferencia estadoDeLasPreferencias)
        {
            var actualizarContadorDePreferencias = new GuardarPreferencias
            {
                ContadorDePreferencias = ++estadoDeLasPreferencias.ContadorPreferenciasElegidas
            };

            return actualizarContadorDePreferencias;
        }

        private OperationResult<GuardarPreferencias> SetResponsePreferencia(GuardarPreferencias preferenciasUsuario)
        {
            var responsePreferencia = new OperationResult<GuardarPreferencias>
            {
                Data = preferenciasUsuario,
            };

            return responsePreferencia;
        }

        public OperationResult<GuardarPreferencias> GuardarPreferencia(GuardarPreferencias preferenciasUsuario, int loggedUserId)
        {
            try
            {
                var estadoDeLasPreferencias = GetEstadoDePreferencias(preferenciasUsuario);
                AddRespuestaUsuariosGrupos(preferenciasUsuario, loggedUserId);
                var actualizarContadorDePreferencias = UpdateContadorDePreferencias(estadoDeLasPreferencias);
                var responsePreferencia = SetResponsePreferencia(preferenciasUsuario);
                context.SaveChanges();

                return responsePreferencia;
            }
            catch (Exception ex)
            {
                var responsePreferencia = new OperationResult<GuardarPreferencias>
                {
                    MensajePersonalizado = ex.Message
                };

                return responsePreferencia;
            }
        }
    }
}