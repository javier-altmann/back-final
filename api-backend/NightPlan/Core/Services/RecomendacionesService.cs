using System.Linq;
using Core.DTO;
using Core.Interfaces;
using DAL.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using DAL;
using Core.Services.ResponseModels;
using System;

namespace Core.Services
{
    public class RecomendacionesService : IRecomendadosService
    {
        private NightPlanDbContext context;
        public RecomendacionesService(NightPlanDbContext context)
        {
            this.context = context;
        }

        public OperationResult<IEnumerable<EstablecimientoDTO>> GetEstablecimientosRecomendados(int grupoId)
        {
            var listaRecomendadosWithScoring = GetEstablecimientosWithScoring(grupoId);
            var listaRecomendados = listaRecomendadosWithScoring
                .Take(10)
                .ToList();

            return new OperationResult<IEnumerable<EstablecimientoDTO>>(listaRecomendados);
        }

        private IEnumerable<EstablecimientoConScoringDTO> GetEstablecimientosWithScoring(int grupoId)
        {
            var getScoring = GetScoringByGrupo(grupoId);
            return context.EstablecimientosCaracteristicas
                .Include(x => x.Caracteristica)
                .Include(x => x.Establecimiento)
                .ThenInclude(x => x.Barrio)
                .GroupBy(x => x.Establecimiento.Id)
                .AsEnumerable()
                .Select(ep =>
                {
                    var establecimiento = ep.First().Establecimiento;
                    return new EstablecimientoConScoringDTO
                    {
                        Id = establecimiento.Id,
                        Nombre = establecimiento.Nombre,
                        Imagen = establecimiento.Imagen,
                        Direccion = establecimiento.Direccion,
                        Destacado = establecimiento.Destacado,
                        Score = getScoring(establecimiento),
                    };
                })
                .OrderByDescending(x => x.Score);
        }
        
        private Func<Establecimiento, int> GetScoringByGrupo(int grupoId)
        {
            var respuestasUsuariosPreferencias = context.PreferenciasCaracteristicas
                .Where(x => x.GrupoUsuario.Grupo.Id == grupoId)
                .GroupBy(x => x.Caracteristica.Id)
                .ToDictionary(x => x.Key, x => x.Count());
                 
            var respuestasUsuariosBarrios = context.PreferenciasBarriales
                .Where(x => x.GrupoUsuario.Grupo.Id == grupoId)
                .GroupBy(x => x.Barrio.Id)
                .ToDictionary(x => x.Key, x => x.Count());
            return establecimiento => GetScoring(establecimiento, respuestasUsuariosPreferencias, respuestasUsuariosBarrios );
        }

        private int GetScoring(Establecimiento establecimiento, Dictionary<int, int> respuestasUsuarios, Dictionary<int, int> respuestasBarriosIds)
        {
            var result = respuestasBarriosIds.ContainsKey(establecimiento.Barrio.Id)
                ? respuestasBarriosIds[establecimiento.Barrio.Id]
                : 0;
            return result + establecimiento.EstablecimientoCaracteristicas
                .Select(eP => eP.Caracteristica.Id)
                .Select(id => respuestasUsuarios.ContainsKey(id)
                    ? respuestasUsuarios[id]
                    : 0)
                    .Sum();
        }
    }
}