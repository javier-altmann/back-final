using Core.DTO;

namespace Core.Services.ResponseModels
{
    public class LoginResponseApi : BaseDTO
    {
        public string Message { get; set; }
        public bool Succes { get; set; }

    }
}