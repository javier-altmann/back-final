using System.Collections.Generic;
using System.Linq;

namespace Core.Services.ResponseModels
{
    public class OperationResult<T>
    {
        public OperationResult()
        {

        }
        
        public OperationResult(T result)
        {
            Data = result;
        }

        public T Data { get; set; }
        public string Message => Data == null
        ? "La consulta no produjo resultados." : string.Empty;
        public string MensajePersonalizado { get; set; }
    }
}