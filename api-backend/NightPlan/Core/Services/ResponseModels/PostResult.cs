namespace Core.Services.ResponseModels
{
    public class PostResult<T>
    {
        public string MensajePersonalizado { get; set; }    
        public T Data {get; set; }
        public string Message => Data == null 
        ? MensajePersonalizado : string.Empty;     
    }
}