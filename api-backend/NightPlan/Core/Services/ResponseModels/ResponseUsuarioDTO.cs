using Core.DTO;

namespace Core.Services.ResponseModels
{
    public class ResponseUsuarioDTO : BaseDTO
    {
        public string Mail { get; set; }
    }
}