using System.Collections.Generic;
using Core.DTO;

namespace Core.Services.ResponseModels
{
    public class UsuarioDelGrupoResponse
    {
        public UsuarioDelGrupo RespuestaLoguedUser { get; set; }
        public List<UsuarioDelGrupo> Respuestas { get; set; }
    }
}