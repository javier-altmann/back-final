using System;
using System.Collections.Generic;
using System.Linq;
using Core.DTO;
using Core.Interfaces;
using Core.Services.ResponseModels;
using DAL;
using DAL.Model;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Core.Services {
    public class UsuariosService : IUsuariosService {
        private NightPlanDbContext context;

        public UsuariosService (NightPlanDbContext context) {
            this.context = context;
        }

        public OperationResult<IEnumerable<UsuarioDTO>> List (string query = null, int? excludeUserId = null) {
            var listaDeUsuarios = context.Usuarios
                .Where (x => x.Id != excludeUserId &&
                    (query == null ||
                        x.Email.ToUpper ().Contains (query.ToUpper ())))
                .Select (user => new UsuarioDTO () {
                    Id = user.Id,
                        Nombre = user.Nombre,
                        Apellido = user.Apellido,
                        Email = user.Email,
                        FechaNacimiento = user.FechaNacimiento
                }).
            ToList ();
            OperationResult<IEnumerable<UsuarioDTO>> operation = new OperationResult<IEnumerable<UsuarioDTO>> ();
            if (listaDeUsuarios != null) {
                operation.Data = listaDeUsuarios;
                return operation;
            }

            return operation;
        }

        public LoginResponseApi AutenticarUsuarioAdmin (LoginWebDTO usuario) {
            bool autenticacionResponse = context.UsuariosAdministradores.Any (x => x.Username == usuario.Username && x.Password == usuario.Password);
            var id = context.UsuariosAdministradores.Where (x => x.Username == usuario.Username).Select (x => x.Id).FirstOrDefault ();
            if (autenticacionResponse) {
                LoginResponseApi user = new LoginResponseApi () {
                    Id = id,
                    Message = "EL usuario es correcto",
                    Succes = true
                };
                return user;
            }

            LoginResponseApi userError = new LoginResponseApi () {
                Id = id,
                Message = "EL usuario es incorrecto",
                Succes = false
            };
            return userError;
        }

        public Usuario AutenticarUsuario (LoginDTO usuario) {
            return context.Usuarios.SingleOrDefault (x => x.Email == usuario.Email && x.Password == usuario.Password);
        }

        //Trae los datos del usuario para que después pueda editarlo
        public OperationResult<UsuarioDTO> Get (int id) {
            var datosDelUsuario = context.Usuarios.Where (x => x.Id == id)
                .Select (user => new UsuarioDTO () {
                    Id = user.Id,
                        Nombre = user.Nombre,
                        Apellido = user.Apellido,
                        Email = user.Email,
                        FechaNacimiento = user.FechaNacimiento,
                        ImagenPerfil = user.ImagenPerfil,
                }).
            FirstOrDefault ();
            OperationResult<UsuarioDTO> operation = new OperationResult<UsuarioDTO> ();
            if (datosDelUsuario != null) {
                operation.Data = datosDelUsuario;
                return operation;
            }

            return operation;
        }


        private Usuario DtoToModel (UsuarioDTO usuarioDto, Usuario instance = null) {
            instance = instance ?? new Usuario ();
            instance.Nombre = usuarioDto.Nombre;
            instance.Apellido = usuarioDto.Apellido;
            instance.Email = usuarioDto.Email;

            if (usuarioDto.Password != null) {
                instance.Password = usuarioDto.Password;
            }

            instance.FechaNacimiento = usuarioDto.FechaNacimiento;

            if (usuarioDto.ImagenPerfil != null) {
                instance.ImagenPerfil = usuarioDto.ImagenPerfil;
            }

            return instance;
        }

        public Usuario Create (UsuarioDTO user) {
            try {
                var usuario = DtoToModel (user);

                context.Usuarios.Add (usuario);
                context.SaveChanges ();

                return usuario;
            } catch (Exception ex) {
                return null;
            }
        }

        public PostResult<UsuarioDTO> Update (UsuarioDTO user) {
            try {
                var usuario = context.Usuarios.SingleOrDefault (x => x.Id == user.Id);
                DtoToModel (user, usuario);
                context.SaveChanges ();

                return new PostResult<UsuarioDTO> {
                    Data = user
                };
            } catch (Exception ex) {
                return new PostResult<UsuarioDTO> {
                    MensajePersonalizado = ex.Message
                };
            }
        }
    }
}