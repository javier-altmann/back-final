using System;
using System.Collections.Generic;
using System.Linq;
using Core.DTO;
using Core.DTO.Votacion;
using Core.Interfaces;
using Core.Services.ResponseModels;
using DAL.Model;
using DAL;

namespace Core.Services
{
    public class VotacionService : IVotacionService
    {
        private NightPlanDbContext context;
        public VotacionService(NightPlanDbContext context)
        {
            this.context = context;
        }

        public PostResult<VotacionDTO> GuardarVotacion(VotacionDTO votacion)
        {
            try
            {
                var grupo = context.Grupos.SingleOrDefault(x => x.Id == votacion.IdGrupo);
                var establecimiento = context.Establecimientos.SingleOrDefault(x => x.Id == votacion.IdEstablecimiento);
                votacion.FechaDeRespuesta = DateTime.Today;
                var votoDelUsuario = new Votacion
                {
                    Grupo = grupo,
                    Establecimiento = establecimiento,
                    FechaDeRespuesta = votacion.FechaDeRespuesta
                };
                var estadoDeLosVotos = context.EstadoDePreferencias.Where(x => x.Grupo.Id == votacion.IdGrupo)
                                      .FirstOrDefault();
                var actualizarContadorDeVotos = new EstadoDePreferencia{
                    ContadorDeVotos = ++estadoDeLosVotos.ContadorDeVotos
                };

                context.Votaciones.Add(votoDelUsuario);
                estadoDeLosVotos.ContadorDeVotos = actualizarContadorDeVotos.ContadorDeVotos;

                context.SaveChanges();

                var responseVotacion = new PostResult<VotacionDTO>
                {
                    Data = votacion,
                };

                return responseVotacion;

            }
            catch (Exception ex)
            {
                var responseVotacion = new PostResult<VotacionDTO>
                {
                    MensajePersonalizado = ex.Message,
                };
                return responseVotacion;
            }
        }

        private bool GetEstadoDeLaVotacion(int id_grupo)
        {
            var EstadoDeVotos = context.EstadoDePreferencias.Where(x => x.Grupo.Id == 1)
                                                 .Select(x => new EstadoDeVotosDTO
                                                 {
                                                     CantidadDeUsuariosPorGrupo = x.CantidadUsuariosPorGrupo,
                                                     ContadorDeVotos = x.ContadorDeVotos
                                                 }).FirstOrDefault();
            if (EstadoDeVotos.CantidadDeUsuariosPorGrupo == EstadoDeVotos.CantidadDeUsuariosPorGrupo)
            {
                return true;
            }

            return false;

        }
        public OperationResult<List<SugerenciaDTO>> GetResultadoDeLaVotacion(int id_grupo)
        {
            if(GetEstadoDeLaVotacion(1)){
                var votos = context.Votaciones.Where(x=> x.Grupo.Id == id_grupo).ToList();
                var agruparVotos =  votos.GroupBy(x=> x.Establecimiento.Id)
                .Select(c=> new SugerenciaDTO{
                    IdEstablecimiento = c.Key,
                    CantidadDeVotos = c.Count()
                }).
                OrderByDescending(x=> x.CantidadDeVotos).ToList();
                agruparVotos[0].gano = true;
                var operationResult = new OperationResult<List<SugerenciaDTO>>();
                operationResult.Data = agruparVotos;

                
                return operationResult;                 
            }
            return new OperationResult<List<SugerenciaDTO>>();
        }

    }
}
