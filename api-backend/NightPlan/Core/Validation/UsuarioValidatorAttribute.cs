using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using Core.DTO;
using Core.Services;
using DAL;
using DAL.Model;

namespace Core.Validation
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class UsuarioValidatorAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dbContext = (NightPlanDbContext)validationContext.GetService(typeof(NightPlanDbContext));
            var dto = validationContext.ObjectInstance;
            var user = dto as UsuarioDTO;
            if (user == null)
            {
                throw new InvalidOperationException("No se puede aplicar el validator unique email a una clase distinta de" + typeof(UsuarioDTO).Name);
            }
            var result = dbContext.Usuarios.Any(x => x.Email == user.Email && x.Id != user.Id);
            return result ? new ValidationResult($"El email: {user.Email} ya existe", new[]{nameof(UsuarioDTO.Email)}) : null;            
        }
    }
}