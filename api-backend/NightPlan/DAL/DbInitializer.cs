﻿using System;
using System.Linq;
using DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DAL
{
    public static class DbInitializer
    {
        public static void Initialize(NightPlanDbContext context)
        {
            context.Database.EnsureCreated();
            if (context.Caracteristicas.Any()) return;

            var preferencias = new Caracteristica[]
            {
                new Instalacion { Nombre = "Aire Acondicionado" },
                new Gastronomia { Nombre = "Comida Arabe" },
            };
            context.Caracteristicas.AddRange(preferencias);
            context.SaveChanges();

            var barrios = new []
            {
                new Barrio
                {
                    Nombre = "Almagro",
                },
                new Barrio
                {
                    Nombre = "Belgrano",
                },
            };

            context.Barrios.AddRange(barrios);
            context.SaveChanges();


            var usuarios = new []
            {
                new Usuario
                {
                    Nombre = "Admin",
                    Apellido = "Apellido",
                    Email = "admin@admin.com",
                    Password = "password",
                    FechaNacimiento = DateTime.Today.AddYears(-30),
                },
                new Usuario
                {
                    Nombre = "OtroUser",
                    Apellido = "OtroApellido",
                    Email = "apellido_otro@admin.com",
                    Password = "123456",
                    FechaNacimiento = DateTime.Today.AddYears(-40),
                },
            };
            context.Usuarios.AddRange(usuarios);
            context.SaveChanges();

            var establecimientos = new []
            {
                new Establecimiento
                {
                    Nombre = "Establecimiento De Prueba",
                    Imagen = "https://img.guiaoleo.com.ar/unsafe/160x120/filters:fill(green)/Basta-Pablo-12702-956f1c44-c7ca-44e9-97a6-94d2ae57739b",
                    Direccion = "Perón 3131",
                    Destacado = true,
                    Barrio = barrios[0],
                }
            };
            context.Establecimientos.AddRange(establecimientos);
            context.SaveChanges();

            var establecimientosPreferencias = new []
            {
                new EstablecimientoCaracteristica
                {
                    Caracteristica = preferencias[0],
                    Establecimiento = establecimientos[0],
                },
                new EstablecimientoCaracteristica
                {
                    Caracteristica = preferencias[1],
                    Establecimiento = establecimientos[0],
                }
            };
            context.EstablecimientosCaracteristicas.AddRange(establecimientosPreferencias);

            context.SaveChanges();            
        }
    }
}