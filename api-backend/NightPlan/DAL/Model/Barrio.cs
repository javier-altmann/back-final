﻿using System.Collections.Generic;

namespace DAL.Model
{
    public class Barrio : BaseEntity
    {
        public string Nombre { get; set; }
    }
}
