﻿using System.Collections.Generic;

namespace DAL.Model
{
    public abstract class Caracteristica : BaseEntity
    {
        public string Nombre { get; set; }
        public virtual IList<EstablecimientoCaracteristica> EstablecimientosCaracteristicas { get; set; } = new List<EstablecimientoCaracteristica>();
    }
}
