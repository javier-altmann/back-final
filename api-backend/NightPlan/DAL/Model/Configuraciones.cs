﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class Configuracion : BaseEntity
    {
        public string DireccionEstablecimientos { get; set; }
        public string DireccionFotosUsuarios { get; set; }
    }
}
