﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class Establecimiento : BaseEntity
    {
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Imagen { get; set; }
        public bool Destacado { get; set; }
        public virtual IList<Votacion> Votaciones { get; set; } = new List<Votacion>();
        public virtual IList<EstablecimientoCaracteristica> EstablecimientoCaracteristicas { get; set; } = new List<EstablecimientoCaracteristica>();
        public Barrio Barrio { get; set; }
    }
}
