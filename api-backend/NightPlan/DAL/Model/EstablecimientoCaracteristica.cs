﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class EstablecimientoCaracteristica : BaseEntity
    {
        public virtual Establecimiento Establecimiento { get; set; }
        public virtual Caracteristica Caracteristica { get; set; }
    }
}
