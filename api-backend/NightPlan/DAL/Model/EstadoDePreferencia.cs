﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class EstadoDePreferencia : BaseEntity
    {
        public int? ContadorPreferenciasElegidas { get; set; }
        public int? CantidadUsuariosPorGrupo { get; set; }
        public int? ContadorDeVotos { get; set; }
        public Grupo Grupo { get; set; }
    }
}
