﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class Grupo : BaseEntity
    {
        public string Nombre { get; set; }
        public string Imagen { get; set; }
        public DateTime? FechaCreacion { get; set; }

        public virtual IList<EstadoDePreferencia> EstadoDePreferencias { get; set; } = new List<EstadoDePreferencia>();
        public virtual IList<GrupoUsuario> GruposUsuarios { get; set; } = new List<GrupoUsuario>();
        public IList<Votacion> Votaciones { get; set; } = new List<Votacion>();
    }
}
