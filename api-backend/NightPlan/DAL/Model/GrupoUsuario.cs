﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class GrupoUsuario : BaseEntity
    {
        public Grupo Grupo { get; set; }
        public Usuario Usuario { get; set; }
        public bool Respondio { get; set; }
        public virtual IList<GrupoUsuarioPreferencia> GruposUsuariosPreferencias { get; set; } = new List<GrupoUsuarioPreferencia>();
    }
}
