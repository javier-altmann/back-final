﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public abstract class GrupoUsuarioPreferencia : BaseEntity
    {
        public virtual GrupoUsuario GrupoUsuario { get; set; }
    }
}
