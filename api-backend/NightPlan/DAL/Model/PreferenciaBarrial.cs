﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class PreferenciaBarrial : GrupoUsuarioPreferencia
    {
        public virtual Barrio Barrio { get; set; }
    }
}
