﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class PreferenciaCaracteristica : GrupoUsuarioPreferencia
    {
        public virtual Caracteristica Caracteristica { get; set; }
    }
}
