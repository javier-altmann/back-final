﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class Usuario : BaseEntity
    {        
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string ImagenPerfil { get; set; }
        public virtual IList<GrupoUsuario> GruposUsuarios { get; set; } = new List<GrupoUsuario>();
    }
}
