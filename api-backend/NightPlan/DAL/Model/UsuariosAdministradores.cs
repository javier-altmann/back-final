﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class UsuarioAdministrador : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
    }
}
