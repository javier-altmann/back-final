﻿using System;
using System.Collections.Generic;

namespace DAL.Model
{
    public class Votacion : BaseEntity
    {
        public DateTime? FechaDeRespuesta { get; set; }

        public Establecimiento Establecimiento { get; set; }
        public Grupo Grupo { get; set; }
    }
}
