using DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DAL
{
    public class NightPlanContextFactory : IDesignTimeDbContextFactory<NightPlanDbContext>
    {        
        public NightPlanContextFactory()
        {

        }

        public NightPlanDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<NightPlanDbContext>();
            
            var connection = "Server=localhost;User Id=root;Password=root;Database=nightPlan;port=3306";
           
            optionsBuilder.UseMySql(connection);

            return new NightPlanDbContext(optionsBuilder.Options);
        }
    }
}