﻿using System;
using DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DAL
{
    public class NightPlanDbContext : DbContext
    {
        public virtual DbSet<Barrio> Barrios { get; set; }
        public virtual DbSet<Instalacion> Instalaciones { get; set; }
        public virtual DbSet<Caracteristica> Caracteristicas { get; set; }
        public virtual DbSet<Configuracion> Configuraciones { get; set; }
        public virtual DbSet<EstablecimientoCaracteristica> EstablecimientosCaracteristicas { get; set; }
        public virtual DbSet<Establecimiento> Establecimientos { get; set; }
        public virtual DbSet<EstadoDePreferencia> EstadoDePreferencias { get; set; }
        public virtual DbSet<Gastronomia> Gastronomias { get; set; }
        public virtual DbSet<Grupo> Grupos { get; set; }
        public virtual DbSet<GrupoUsuario> GruposUsuarios { get; set; }
        public virtual DbSet<GrupoUsuarioPreferencia> GruposUsuariosPreferencias { get; set; }
        public virtual DbSet<PreferenciaBarrial> PreferenciasBarriales { get; set; }
        public virtual DbSet<PreferenciaCaracteristica> PreferenciasCaracteristicas { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<UsuarioAdministrador> UsuariosAdministradores { get; set; }
        public virtual DbSet<Votacion> Votaciones { get; set; }

        public NightPlanDbContext(DbContextOptions<NightPlanDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Caracteristica>(entity =>
            {
                entity.ToTable(nameof(Caracteristicas));
                entity.HasMany(x => x.EstablecimientosCaracteristicas)
                    .WithOne(x => x.Caracteristica);
            });

            modelBuilder.Entity<Configuracion>(entity =>
            {
                entity.ToTable(nameof(Configuraciones));

                entity.Property(e => e.DireccionEstablecimientos)
                    .HasMaxLength(500);

                entity.Property(e => e.DireccionFotosUsuarios)
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Establecimiento>(entity =>
            {
                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Imagen)
                    .HasMaxLength(int.MaxValue);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(80);
            });

            modelBuilder.Entity<EstadoDePreferencia>(entity =>
            {
                entity.ToTable(nameof(EstadoDePreferencias));
                entity.HasOne(d => d.Grupo)
                    .WithMany(p => p.EstadoDePreferencias);
            });

            modelBuilder.Entity<Grupo>(entity =>
            {
                entity.Property(e => e.Imagen)
                    .HasMaxLength(int.MaxValue);
            });

            modelBuilder.Entity<GrupoUsuario>(entity =>
            {
                entity.ToTable(nameof(GruposUsuarios));
                entity.HasOne(d => d.Grupo)
                    .WithMany(p => p.GruposUsuarios);

                entity.HasOne(d => d.Usuario)
                    .WithMany(p => p.GruposUsuarios);
            });

            modelBuilder.Entity<GrupoUsuarioPreferencia>(entity => 
            {
                entity.ToTable(nameof(GruposUsuariosPreferencias));
                entity.HasOne(x => x.GrupoUsuario)
                    .WithMany(x => x.GruposUsuariosPreferencias);
            });
            
            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.Property(e => e.Apellido)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Email)
                    .HasMaxLength(100);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ImagenPerfil)
                    .HasMaxLength(int.MaxValue);
            });

            modelBuilder.Entity<UsuarioAdministrador>(entity =>
            {
                entity.Property(e => e.Apellido)
                    .HasMaxLength(150);

                entity.Property(e => e.Email)
                    .HasMaxLength(150);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(150);

                entity.Property(e => e.Password)
                    .HasMaxLength(150);

                entity.Property(e => e.Username)
                    .HasMaxLength(150);
            });

            modelBuilder.Entity<Votacion>(entity =>
            {
                entity.ToTable(nameof(Votaciones));

                entity.HasOne(d => d.Establecimiento)
                    .WithMany(p => p.Votaciones);

                entity.HasOne(d => d.Grupo)
                    .WithMany(p => p.Votaciones);
            });
        }
    }
}