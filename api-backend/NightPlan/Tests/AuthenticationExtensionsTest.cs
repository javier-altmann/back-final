using System.Threading.Tasks;
using Api;
using Core.DTO;
using Test;
using Xunit;

namespace Tests
{
    public class AuthenticationExtensionsTest : BaseDbTests
    {
        [Fact]
        public async Task when_hash_a_password()
        {
            LoginDTO login = new LoginDTO
            {
                Password = "test de hash"
            };
            
            string hashPassword =  login.Password.ComputeSha256Hash();
            Assert.StrictEqual("00ff42acb8871d5d2a3c143ae6bbe6bb94793155f9b2de6baa458d059016d8d9", hashPassword);
        }
    }
}