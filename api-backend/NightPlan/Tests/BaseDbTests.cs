using Xunit;
using System.Threading.Tasks;
using Core.Services;
using Microsoft.EntityFrameworkCore;
using DAL;
using Microsoft.Extensions.DependencyInjection;
using DAL.Model;
using FluentAssertions;

namespace Test
{
    public abstract class BaseDbTests
    {
        protected ServiceProvider serviceProvider;
        protected IServiceCollection serviceCollection;
        protected NightPlanDbContext seedContext;
        protected NightPlanDbContext testContext;
        
        public BaseDbTests()
        {
            CreateServices();

            
            this.seedContext = serviceProvider.GetService<NightPlanDbContext>();
            this.testContext = serviceProvider.GetService<NightPlanDbContext>();
        }

        protected async Task<TEntity> CreateAsync<TEntity>(TEntity baseData = null)
            where TEntity: BaseEntity, new()
        {
            var dbSet = this.seedContext.Set<TEntity>();
            var result = baseData ?? new TEntity();
            dbSet.Add(result);
            await this.seedContext.SaveChangesAsync();
            return result;
        }
        
        private void CreateServices()
        {
            // Create a fresh service provider, and therefore a fresh
            // InMemory database instance.
            this.serviceCollection = new ServiceCollection();
            
            this.serviceCollection.AddDbContext<NightPlanDbContext>(x =>
            {
                x.UseInternalServiceProvider(serviceProvider)
                .UseInMemoryDatabase("nightplan_db");
            });
            
            this.serviceProvider = serviceCollection
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
        }
    }
}