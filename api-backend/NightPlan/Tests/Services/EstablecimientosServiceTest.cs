using Xunit;
using System.Threading.Tasks;
using Core.Services;
using Microsoft.EntityFrameworkCore;
using DAL;
using Microsoft.Extensions.DependencyInjection;
using DAL.Model;
using FluentAssertions;
using System.Linq;
using Core.Validation;
using Core.DTO;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using Test;
using Core.Interfaces;

namespace Tests.Core.Services
{
    public class EstablecimientosServiceTest : BaseDbTests
    {
        private IEstablecimientosService subject;

        public EstablecimientosServiceTest() : base()
        {
            subject = new EstablecimientosService(testContext);
        }

        [Fact]
        public async Task when_get_caracteristicas_instalaciones()
        {
            var instalacion = await CreateAsync(new Instalacion { Nombre = "Escalera mecánica" });
            var gastronomia = await CreateAsync(new Gastronomia { Nombre = "Comida Rusa" });
            var result = subject.GetInstalaciones();

            result.Count().Should().Be(1, "it get a single instalacion");
            result.First().Nombre.Should().Be("Escalera mecánica");
        }

        [Fact]
        public async Task when_get_caracteristicas_gastronomia()
        {
            var instalacion = await CreateAsync(new Instalacion { Nombre = "Juegos infantiles" });
            var gastronomia = await CreateAsync(new Gastronomia { Nombre = "Comida Japonesa" });
            var result = subject.GetGastronomia();

            result.Count().Should().Be(1, "it get a single instalacion");
            result.First().Nombre.Should().Be("Comida Japonesa");
        }
    }
}