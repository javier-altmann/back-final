using Xunit;
using System.Threading.Tasks;
using Core.Services;
using Microsoft.EntityFrameworkCore;
using DAL;
using Microsoft.Extensions.DependencyInjection;
using DAL.Model;
using FluentAssertions;
using System.Linq;
using Core.Validation;
using Core.DTO;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using Test;

namespace Tests.Core.Services
{
    public class PreferenciasServiceTests : BaseDbTests
    {
        private PreferenciasService subject;

        public PreferenciasServiceTests() : base()
        {
            subject = new PreferenciasService(testContext);
        }

        [Fact]
        public async Task when_saving_one_user_preferences()
        {
            var grupo = await CreateAsync<Grupo>();
            var user = await CreateAsync<Usuario>();
            var barrio1 = await CreateAsync<Barrio>();
            var barrio2 = await CreateAsync<Barrio>();
            var instalacion = await CreateAsync<Instalacion>();
            var gastronomia = await CreateAsync<Gastronomia>();

            var grupoUsuarioPreferencias = await CreateAsync(new GrupoUsuario{ Grupo = grupo, Usuario = user});
            var estado = await CreateAsync(new EstadoDePreferencia{ Grupo = grupo });

            var setPreferencias = new GuardarPreferencias
            {
                GrupoId = grupo.Id,
                Respuesta = new PreferenciasDTO
                {
                    IdsGastronomia = new List<int>{ gastronomia.Id },
                    IdsBarrios = new List<int>{ barrio1.Id, barrio2.Id },
                    IdsCaracteristicas = new List<int>{ instalacion.Id },
                },
            };

            subject.GuardarPreferencia(setPreferencias, user.Id);

            var result = await testContext.GruposUsuarios
                .Include(x => x.GruposUsuariosPreferencias)
                .ThenInclude(x => (x as PreferenciaBarrial).Barrio)
                .Include(x => x.GruposUsuariosPreferencias)
                .ThenInclude(x => (x as PreferenciaCaracteristica).Caracteristica)
                .SingleOrDefaultAsync(x => x.Usuario.Id == user.Id);

            var preferencias = result.GruposUsuariosPreferencias;
            preferencias.Count().Should().Be(4, "it should set the 4 preferencias");                        
            (preferencias[0] as PreferenciaCaracteristica).Caracteristica.Id.Should().Be(gastronomia.Id);
            (preferencias[1] as PreferenciaCaracteristica).Caracteristica.Id.Should().Be(instalacion.Id);
            (preferencias[2] as PreferenciaBarrial).Barrio.Id.Should().Be(barrio1.Id);
            (preferencias[3] as PreferenciaBarrial).Barrio.Id.Should().Be(barrio2.Id);
        }
    }
}