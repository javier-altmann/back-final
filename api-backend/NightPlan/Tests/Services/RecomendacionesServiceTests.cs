using Xunit;
using System.Threading.Tasks;
using Core.Services;
using Microsoft.EntityFrameworkCore;
using DAL;
using Microsoft.Extensions.DependencyInjection;
using DAL.Model;
using FluentAssertions;
using System.Linq;
using Core.Validation;
using Core.DTO;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using Test;

namespace Tests.Core.Services
{
    public class RecomendacionesServiceTest : BaseDbTests
    {
        private RecomendacionesService subject;

        public RecomendacionesServiceTest() : base()
        {
            subject = new RecomendacionesService(testContext);
        }

        [Fact]
        public async Task when_having_one_grupo_and_one_establecimiento_no_preferencias_elegidas()
        {
            var grupo = await CreateAsync<Grupo>();
            
            var barrio = await CreateAsync(new Barrio{ Nombre = "Almagro" });
            var establecimiento = await CreateAsync(new Establecimiento { Barrio = barrio });
            var preferencia = await CreateAsync(new Instalacion{ Nombre = "Juegos para niños" });
            var eP = await CreateAsync(new EstablecimientoCaracteristica
            {
                Establecimiento = establecimiento,
                Caracteristica = preferencia,
            });
            var result = subject.GetEstablecimientosRecomendados(grupo.Id);
            var list = result.Data.ToList();
            list.Count.Should().Be(1, "It should return the only establecimiento");
        }
        
        [Fact]
        public async Task when_having_3_establecimientos_3_preferencias_3_usuarios()
        {
            var grupo = await CreateAsync<Grupo>();

            var barrio1 = await CreateAsync<Barrio>();
            var barrio2 = await CreateAsync<Barrio>();
            var barrio3 = await CreateAsync<Barrio>();
            
            var usuario1 = await CreateAsync<Usuario>();
            var usuario2 = await CreateAsync<Usuario>();
            var usuario3 = await CreateAsync<Usuario>();

            var gU1 = await CreateAsync(new GrupoUsuario { Usuario = usuario1, Grupo = grupo});
            var gU2 = await CreateAsync(new GrupoUsuario { Usuario = usuario2, Grupo = grupo});
            var gU3 = await CreateAsync(new GrupoUsuario { Usuario = usuario3, Grupo = grupo});

            var establecimiento1 = await CreateAsync(new Establecimiento{ Barrio = barrio1 });
            var establecimiento2 = await CreateAsync(new Establecimiento{ Barrio = barrio2 });
            var establecimiento3 = await CreateAsync(new Establecimiento{ Barrio = barrio3 });

            var instalacion = await CreateAsync<Instalacion>();
            var gastronomia = await CreateAsync<Gastronomia>();
            
            var e3P2 = await CreateAsync(
                new EstablecimientoCaracteristica { Establecimiento = establecimiento3, Caracteristica = instalacion });
            var e2P3 = await CreateAsync(
                new EstablecimientoCaracteristica { Establecimiento = establecimiento2, Caracteristica = gastronomia });
            var e1P2 = await CreateAsync(
                new EstablecimientoCaracteristica { Establecimiento = establecimiento1, Caracteristica = instalacion });
           
            var gU1p3 = await CreateAsync(
                new PreferenciaCaracteristica{ GrupoUsuario = gU1, Caracteristica = gastronomia });
            var gU1p2 = await CreateAsync(
                new PreferenciaCaracteristica{ GrupoUsuario = gU1, Caracteristica = instalacion });

            var gU1p1b2 = await CreateAsync(
               new PreferenciaBarrial { GrupoUsuario = gU1, Barrio = barrio2 });
            
            var gU1p1b3 = await CreateAsync(
               new PreferenciaBarrial { GrupoUsuario = gU1, Barrio = barrio3 });

            var gU2p2 = await CreateAsync(
                new PreferenciaCaracteristica{ GrupoUsuario = gU2, Caracteristica = instalacion });
           
            var gU2p1b2 = await CreateAsync(
               new PreferenciaBarrial { GrupoUsuario = gU2, Barrio = barrio2});
            var gU2p3 = await CreateAsync(
                new PreferenciaCaracteristica { GrupoUsuario = gU2, Caracteristica = gastronomia });
            
            var gU3p1b1 = await CreateAsync(
               new PreferenciaBarrial { GrupoUsuario = gU3, Barrio = barrio1 });
            var gU3p3 = await CreateAsync(
                new PreferenciaCaracteristica{ GrupoUsuario = gU3, Caracteristica = gastronomia });
            
            var result = subject.GetEstablecimientosRecomendados(grupo.Id);
            var list = result.Data.ToList();
            list.Count.Should().Be(3, "It should return all the 3 establecimientos");
            
            list[0].Id.Should().Be(establecimiento2.Id);
            list[1].Id.Should().Be(establecimiento3.Id);
            list[2].Id.Should().Be(establecimiento1.Id);
        }

        
        [Fact]
        public async Task when_having_1_establecimiento_2_usuarios_2_barrios_2_caracteristicas_5_preferencias()
        {
            var grupo = await CreateAsync<Grupo>();

            var barrio1 = await CreateAsync<Barrio>();
            var barrio2 = await CreateAsync<Barrio>();
            
            var usuario1 = await CreateAsync<Usuario>();
            var usuario2 = await CreateAsync<Usuario>();

            var gU1 = await CreateAsync(new GrupoUsuario { Usuario = usuario1, Grupo = grupo});
            var gU2 = await CreateAsync(new GrupoUsuario { Usuario = usuario2, Grupo = grupo});

            var establecimiento = await CreateAsync(new Establecimiento{ Barrio = barrio1 });

            var instalacion = await CreateAsync<Instalacion>();
            var gastronomia = await CreateAsync<Gastronomia>();
            
            await CreateAsync(
                new EstablecimientoCaracteristica { Establecimiento = establecimiento, Caracteristica = instalacion });
            await CreateAsync(
                new EstablecimientoCaracteristica { Establecimiento = establecimiento, Caracteristica = gastronomia });
            
            await CreateAsync(new PreferenciaBarrial{ GrupoUsuario = gU1, Barrio = barrio1 });
            await CreateAsync(new PreferenciaBarrial{ GrupoUsuario = gU2, Barrio = barrio2 });
        
            await CreateAsync(new PreferenciaCaracteristica{ GrupoUsuario = gU1, Caracteristica = instalacion });
            await CreateAsync(new PreferenciaCaracteristica{ GrupoUsuario = gU1, Caracteristica = gastronomia });
            await CreateAsync(new PreferenciaCaracteristica{ GrupoUsuario = gU2, Caracteristica = gastronomia });
                    
            var result = subject.GetEstablecimientosRecomendados(grupo.Id);
            var list = result.Data.ToList();
            list.Count.Should().Be(1, "It should return 1 establecimiento");
            
            list[0].Id.Should().Be(establecimiento.Id);
        }
    }
}