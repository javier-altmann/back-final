using System;
using System.Linq;
using System.Threading.Tasks;
using Core.DTO;
using Core.Services;
using DAL.Model;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Test;
using Xunit;

namespace Tests.Core.Services
{
    public class UsuariosServiceTest : BaseDbTests
    {
        private UsuariosService subject;

        public UsuariosServiceTest() : base()
        {
            subject = new UsuariosService(testContext);
        }

        [Fact]
        public async Task when_create_one_user()
        {
            var usuario = new UsuarioDTO
            {
                Nombre = "Javier",
                Apellido = "Altmann",
                FechaNacimiento = DateTime.Today,
                Email = "javier.altmann@hotmail.com",
                ImagenPerfil = "imagen.png",
                Password = "javier1245"
            };

            var createResult = subject.Create(usuario);

            var result = await testContext.Usuarios.SingleOrDefaultAsync(x => x.Email == usuario.Email);

            createResult.Id.Should().BeGreaterThan(0, "it should set User Id");
            
            result.Nombre.Should().Be(usuario.Nombre);
            result.Apellido.Should().Be(usuario.Apellido);
            result.FechaNacimiento.Should().Be(usuario.FechaNacimiento);
            result.Email.Should().Be(usuario.Email);
            result.ImagenPerfil.Should().Be(usuario.ImagenPerfil);
            result.Password.Should().Be(usuario.Password);
        }

        [Fact]
        public async Task when_update_one_user()
        {
            var usuario = await CreateAsync(new Usuario
            { 
                Nombre = "Martin",
                Apellido = "Gonzalez",
                FechaNacimiento = DateTime.Today,
                Email = "martin.gonzalez@hotmail.com",
                ImagenPerfil = "imagen.png",
                Password = "martin1245"
            });

            var userDto = new UsuarioDTO
            {
                Id = usuario.Id,
                Nombre = "Juan",
                Apellido = "Gomez",
                FechaNacimiento = DateTime.Today,
                Email = "test.email@hotmail.com",
                ImagenPerfil = "test.png",
                Password = "test1245"
            };

            var userResponse = subject.Update(userDto);

            var result = await testContext.Usuarios.SingleOrDefaultAsync(x => x.Id == usuario.Id);

            result.Nombre.Should().Be(userDto.Nombre);
            result.Apellido.Should().Be(userDto.Apellido);
            result.FechaNacimiento.Should().Be(userDto.FechaNacimiento);
            result.Email.Should().Be(userDto.Email);
            result.ImagenPerfil.Should().Be(userDto.ImagenPerfil);
            result.Password.Should().Be(userDto.Password);
        }

        [Fact]
        public async Task when_update_a_user_without_password()
        {
            var usuario = await CreateAsync(new Usuario
            { 
                Nombre = "Marcela",
                Apellido = "Gomez",
                FechaNacimiento = DateTime.Today,
                Email = "marcela.gomez@hotmail.com",
                ImagenPerfil = "imagen.png",
                Password = "test123"
            });
            
            var userDto = new UsuarioDTO {
                Id = usuario.Id,
                Nombre = "Gonzalo",
                Apellido = "Gimenez",
                FechaNacimiento = DateTime.Today,
                Email = "gonzalo.gimenez@hotmail.com",
                ImagenPerfil = "test.png"
            };

            var userResponse = subject.Update(userDto);

            var result = await testContext.Usuarios.SingleOrDefaultAsync(x => x.Id == usuario.Id);

            result.Nombre.Should().Be(userDto.Nombre);
            result.Apellido.Should().Be(userDto.Apellido);
            result.FechaNacimiento.Should().Be(userDto.FechaNacimiento);
            result.Email.Should().Be(userDto.Email);
            result.ImagenPerfil.Should().Be(userDto.ImagenPerfil);
            result.Password.Should().Be(usuario.Password);
        }

        [Fact]
        public async Task when_update_a_user_without_image()
        {
            var usuario = await CreateAsync(new Usuario{
                Nombre = "Gaston",
                Apellido = "Gimenez",
                FechaNacimiento = DateTime.Today,
                Email = "gaston.gimenez@hotmail.com",
                ImagenPerfil = "image.png",
                Password = "pass1234"    
            });

            var userDto = new UsuarioDTO {
                Id = usuario.Id,
                Nombre = "Gonzalo",
                Apellido = "Gimenez",
                FechaNacimiento = DateTime.Today,
                Email = "gonzalo.gimenez@hotmail.com",
                Password = "pass456"
            };

            var userResponse = subject.Update(userDto);

            var result = await testContext.Usuarios.SingleOrDefaultAsync(x => x.Id == usuario.Id);

            result.Nombre.Should().Be(userDto.Nombre);
            result.Apellido.Should().Be(userDto.Apellido);
            result.FechaNacimiento.Should().Be(userDto.FechaNacimiento);
            result.Email.Should().Be(userDto.Email);
            result.ImagenPerfil.Should().Be(usuario.ImagenPerfil);
            result.Password.Should().Be(usuario.Password);
        }
    }
}