using Xunit;
using System.Threading.Tasks;
using DAL.Model;
using FluentAssertions;
using System.Linq;
using Core.DTO;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Test;

namespace Tests.Core.Validation
{
    public class UsuarioValidatorAttributeTest : BaseDbTests
    {
        [Fact]
        public async Task when_creating_usuario_with_repeated_email()
        {
            var usuarioOriginal = await CreateAsync(
                new Usuario { Nombre = "Nombre", Email = "prueba@gmail.com" });
            
            var target = new UsuarioDTO { Email = "prueba@gmail.com", Nombre = "Nombre" };

            var validationContext = new ValidationContext(target, serviceProvider, null);

            var results = new List<ValidationResult>();

            Validator.TryValidateObject(target, validationContext, results, true);

            results.Count.Should().Be(1);
            results.First().ErrorMessage.Should().Be("El email: prueba@gmail.com ya existe");
        }

        [Fact]
        public async Task when_creating_usuario_with_new_email()
        {
            var usuarioOriginal = await CreateAsync(
                new Usuario { Nombre = "Nombre", Email = "prueba@gmail.com" });
            
            var target = new UsuarioDTO { Email = "otro_mail_diferente@gmail.com", Nombre = "Nombre" };

            var validationContext = new ValidationContext(target, serviceProvider, new Dictionary<object, object>());

            var results = new List<ValidationResult>();

            Validator.TryValidateObject(target, validationContext, results, true);

            results.Count.Should().Be(0, "it should return no users");
        }

        
        [Fact]
        public async Task when_updating_usuario_with_same_email()
        {
            var usuarioOriginal = await CreateAsync(
                new Usuario { Nombre = "Nombre", Email = "prueba@gmail.com" });
            
            var target = new UsuarioDTO { Id = usuarioOriginal.Id, Email = "otro_mail_diferente@gmail.com", Nombre = "Nombre" };

            var validationContext = new ValidationContext(target, serviceProvider, new Dictionary<object, object>());

            var results = new List<ValidationResult>();

            Validator.TryValidateObject(target, validationContext, results, true);

            results.Count.Should().Be(0, "it should return no users");
        }
    }
}